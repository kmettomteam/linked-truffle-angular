// https://ethereum.stackexchange.com/questions/12611/solidity-filling-a-struct-array-containing-itself-an-array
// https://medium.com/robhitchens/solidity-crud-part-1-824ffa69509a#.gvh9pf1gj
// pragma solidity 0.4.25

pragma solidity ^0.5.2;

contract Linked {

    struct Participant {
        uint id;
        string name;
    }

    struct Decision {
        uint id;
        string name;
        string content;
        bool statusAgreed;
        string author;
        string[] participantList;
        mapping(string => Participant) participants;

        // string[] participants;
    }

    // function addParticipant (string memory _name) public {
    //   participantsCount ++;
    //   participants[participantsCount] = Participant(1, _name);
    // }

    function addDecision (string memory _decName, string memory   _participant ) public {
      decisionsCount ++;
      decisions[decisionsCount].name = _decName;
      decisions[decisionsCount].content = "test content decision";
      decisions[decisionsCount].statusAgreed = false;
      decisions[decisionsCount].author = _participant;

      // decisions[decisionsCount].participantList = [_participant];

      decisions[decisionsCount].participantList.push(_participant);
      decisions[decisionsCount].participants[_participant].name = _participant;

      //   addParticipantToDecision( decisionsCount , _participant);
    }

    // function getDecisionParticipants (string memory _decisionKey ) public returns(string[] participantList){
    //   return ( decisions[_decisionKey].participantList );
    // }

    function addParticipantToDecision (uint  _decKey , string memory  _participant  ) public {

      decisions[_decKey].participantList.push(_participant);


    }


    // mapping(uint => Participant) public participants;
    mapping(uint => Decision) public decisions;

    // uint public participantsCount;
    uint public decisionsCount;

    // function vote (uint _candidateId) public {
    //     // require that they haven't voted before
    //     // require(!voters[msg.sender]);

    //     // require a valid candidate
    //     // require(_candidateId > 0 && _candidateId <= participantsCount);

    //     // record that voter has voted
    //     // voters[msg.sender] = true;

    //     // update candidate vote Count
    //     // participants[_candidateId].voteCount ++;

    //     // trigger voted event
    //     // emit votedEvent(_candidateId);
    // }


        // voted event
        event votedEvent (
            uint indexed _participantId
        );

        // constructor () public {
        //     addCandidate("Candidate 1");
        //     addCandidate("Candidate 2");
        // }


}
